{ config, ... }: {
  services.hydra = {
    enable = true;
    hydraURL = "https://hydra.${config.networking.domain}";
    notificationSender = "hydra@${config.networking.domain}";
    useSubstitutes = true;
  };
  nix.allowedUsers = [ "hydra" "hydra-www" "nix-serve" ];
  services.nix-serve = {
    enable = true;
    secretKeyFile = "/var/cache-priv-key.pem";
  };
}
