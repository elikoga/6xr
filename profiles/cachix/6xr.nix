{
  nix = {
    binaryCaches = [
      "https://cache.6xr.de/"
    ];
    binaryCachePublicKeys = [
      "cache.6xr.de-001:L06KL41tHhaE/TJ46blisRlrVaJ8hwPTr8FZaXekyEo="
    ];
  };
}
