rec {
  coafin = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFHtaObi0gAy0sjp9VoidFI6eOnjWHO+yL19+VxXmTR0 coafin@red-fennec"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKhg8pee2fZwv6ADydB9Lxa2xjmJlbwUESFIILyh9PZ+ eli.kogan-wang@weidmueller.com"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEO9rt2tmkMNPVhhKNrwSHSrZH632rbqrEzRVfCQO0gn coafin@sixr"
  ];
  root = coafin;
}
