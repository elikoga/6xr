{ config, lib, pkgs, ... }:
let
  cfg = config.elikoga.services.proxy;
in
{
  imports = [ ./backends ];

  options.elikoga = with types; {
    services.proxy = {
      enable = lib.mkEnableOption "custom proxy options";
      proxyVirtualHosts = mkOption {
        type = attrsOf (
          either
            string # Just proxy the whole domain
            (attrsOf string) # Proxy folder specific
        );
      };
      sslEverywhere = lib.mkEnableOption "SSL for all domains" // {
        default = true;
        example = false;
      };
    };
  };
}
