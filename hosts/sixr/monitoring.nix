{ config, ... }:
{
  services.netdata.enable = true;
  services.nginx.virtualHosts."${config.networking.domain}".locations = {
    "/netdata/" = {
      extraConfig = ''
        auth_request /sso-auth;
        auth_request_set $cookie $upstream_http_set_cookie;
        add_header Set-Cookie $cookie;
      '';
    };
  };

}
