{ ... }:
{
  networking.nat.enable = true;
  networking.nat.internalInterfaces = [ "ve-+" ];
  networking.nat.externalInterface = "ens18";
  networking.nat.forwardPorts = [
    {
      destination = "10.233.1.2:8000";
      proto = "tcp";
      sourcePort = 23142;
    }
  ];
}
