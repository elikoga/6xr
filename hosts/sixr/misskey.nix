{ pkgs, ... }:
let yarnPkg = (pkgs.yarn.override {
  nodejs = pkgs.nodejs-16_x;
});
in
{
  systemd.services.misskey = {
    description = "Misskey";
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" "postgresql.service" "redis.service" ];
    path = [
      pkgs.stdenv
      pkgs.python3
      pkgs.nodejs-16_x
      yarnPkg
    ];

    environment = {
      NODE_ENV = "production";
    };
    serviceConfig = {
      User = "coafin";
      Group = "users";
      ExecStart = "${yarnPkg}/bin/yarn start";
      Restart = "always";
      RestartSec = "10s";
      WorkingDirectory = "/home/coafin/misskey";
    };
  };
}
