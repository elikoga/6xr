{ config, ... }:
{
  services.gitea = {
    enable = true;
    domain = config.networking.domain;
    cookieSecure = true;
    rootUrl = "https://git.${config.networking.domain}/";
    httpPort = 3001;
  };
}
