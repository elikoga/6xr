{ config, pkgs, lib, ... }:
let
  ghost = (import ./ghost-cli { inherit pkgs; }).ghost-cli;

in
{
  users.users.ghostUser = {
    description = "Ghost service user";
    home = "/var/lib/ghost";
    useDefaultShell = true;
    group = "ghostGroup";
    isSystemUser = true;
  };

  users.groups.ghostGroup = { };

  security.sudo.extraRules = [
    {
      users = [ "ghostUser" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ]; # "SETENV" # Adding the following could be a good idea
        }
      ];
    }
  ];

  environment.systemPackages = [
    (pkgs.writeShellScriptBin "nuke-ghost" ''
      rm -rf /var/lib/ghost
      systemctl restart ghost-install
    '')
  ];

  systemd.services.ghost-install = {
    wantedBy = [ "ghost.service" "multi-user.target" ];
    after = [ "network.target" ];
    description = "Install ghost";
    environment = {
      NODE_ENV = "production";
      GHOST_INSTALL = "/var/lib/ghost";
      GHOST_CONTENT = "/var/lib/ghost/content";
    };
    unitConfig = {
      ConditionPathExists = "!/var/lib/ghost";
    };
    path = with pkgs; [
      coreutils
      su-exec
      ghost
      nodejs
      yarn
    ];
    serviceConfig = {
      Type = "oneshot";
      User = "root";
      ExecStart = (pkgs.writeShellScriptBin "install-ghost" ''
        mkdir -p "$GHOST_INSTALL";
        chown ghostUser:ghostGroup "$GHOST_INSTALL";
        su-exec ghostUser ghost install "4.12.1" --db sqlite3 --no-prompt --no-stack --no-setup --dir "$GHOST_INSTALL" --network-timeout 100000;
        cd "$GHOST_INSTALL";
        su-exec ghostUser ghost setup \
          --no-setup-mysql \
          --no-setup-nginx \
          --no-setup-ssl \
          --no-setup-systemd \
          --no-setup-linux-user \
          --no-setup-migrate \
          --no-stack --no-start --no-enable --no-prompt \
          --ip 0.0.0.0 --port 2368 --no-prompt --db sqlite3 --url https://ghost.${config.networking.domain} --dbpath "$GHOST_CONTENT/data/ghost.db";
        su-exec ghostUser ghost config paths.contentPath "$GHOST_CONTENT";
        su-exec ghostUser ln -s config.production.json "$GHOST_INSTALL/config.development.json";
        readlink -f "$GHOST_INSTALL/config.development.json";
        # mv "$GHOST_CONTENT" "$GHOST_INSTALL/content.orig";
        # mkdir -p "$GHOST_CONTENT";
        chown ghostUser:ghostGroup "$GHOST_CONTENT";
        chmod 1777 "$GHOST_CONTENT";
        find ./ -type d -exec chmod 00775 {} \;
        cd "$GHOST_INSTALL/current";
        sqlite3Version="$(node -p 'require("./package.json").optionalDependencies.sqlite3')";
        su-exec ghostUser yarn add "sqlite3@$sqlite3Version"
        su-exec ghostUser yarn cache clean;
        su-exec ghostUser npm cache clean --force;
        npm cache clean --force;
        rm -rv /tmp/yarn* /tmp/v8*
        cd "$GHOST_INSTALL";
        find ./ ! -path "./versions/*" -type f -exec chmod 664 {} \;
      '') + "/bin/install-ghost";
    };
  };

  systemd.services.ghost = {
    wantedBy = [ "multi-user.target" ];
    after = [ "ghost-install.service" "network.target" ];
    description = "Start up ghost";
    environment = {
      NODE_ENV = "production";
      GHOST_INSTALL = "/var/lib/ghost";
      GHOST_CONTENT = "/var/lib/ghost/content";
    };
    path = with pkgs; [
      coreutils
      su-exec
      ghost
      nodejs
      yarn
    ];
    serviceConfig = {
      Type = "exec";
      User = "ghostUser";
      ExecStart = (pkgs.writeShellScriptBin "start-ghost" ''
        cd "$GHOST_INSTALL";
        ghost run
      '') + "/bin/start-ghost";
    };
  };
}
