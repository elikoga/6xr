{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.elikoga.services.nginx;
  nixosCfg = config.services.nginx;

  recommendedProxyConfig = pkgs.writeText "nginx-recommended-proxy-headers.conf" ''
    proxy_set_header        Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;
    proxy_set_header        X-Forwarded-Host $host;
    proxy_set_header        X-Forwarded-Server $host;
  '';

  mkLocations = locations:
    let
      locations' = if builtins.isAttrs locations then locations else {
        "/" = locations;
      };
    in
    {
      # addSSL = lib.mkIf cfg.sslEverywhere true;
      enableACME = lib.mkIf cfg.sslEverywhere true;
      forceSSL = lib.mkIf cfg.sslEverywhere true;
      locations = mapAttrs
        (path: destination: {
          extraConfig = ''
            proxy_pass ${destination};

            # This is a sensible default.
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;

            include ${recommendedProxyConfig};
          '';
        })
        locations';
    };

  mkVirtualHosts = mapAttrs (virtualHost: mkLocations);

in
{
  options.elikoga = with types; {
    services.nginx = {
      enable = lib.mkEnableOption "custom nginx";
      proxyTimeout = mkOption {
        default = nixosCfg.proxyTimeout;
      };
      proxyVirtualHosts = mkOption {
        type = attrsOf (either
          string # Just proxy the whole domain
          (attrsOf string) # Proxy folder specific
        );
      };
      sslEverywhere = lib.mkEnableOption "SSL Everywhere for custom nginx";
    };
  };

  config = mkIf cfg.enable {
    services.nginx = {
      appendHttpConfig = lib.mkIf (!nixosCfg.recommendedProxySettings) ''
        proxy_redirect off;
        proxy_connect_timeout ${cfg.proxyTimeout};
        proxy_send_timeout ${cfg.proxyTimeout};
        proxy_read_timeout ${cfg.proxyTimeout};
        proxy_http_version 1.1;
        include ${recommendedProxyConfig};
      '';
      virtualHosts = mkVirtualHosts cfg.proxyVirtualHosts;
    };
  };
}
