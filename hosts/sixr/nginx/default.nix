{ config, lib, ... }:
{
  imports = [
    ./sso.nix
    ./elikoga.nix
  ];
  elikoga.services.nginx = {
    enable = true;
    proxyVirtualHosts = {
      "${config.networking.domain}" = {
        "/netdata/" = "http://localhost:19999/";
        "/d/" = "http://localhost:3344/";
        "/exam-scheduler/" = "http://localhost:9995/";
      };
      "login.${config.networking.domain}" = "http://localhost:8082/";
      "git.${config.networking.domain}" = "http://localhost:3001/";
      "cache.${config.networking.domain}" = "http://localhost:5000/";
      "hydra.${config.networking.domain}" = "http://localhost:3000/";
      "mk.eliko.ga" = "http://localhost:3012/";
      "r.coaf.in" = "http://localhost:5223/";
      "corporate-synergy.6xr.de" = "http://localhost:31245/";
      "lsv.${config.networking.domain}" = "http://78.46.28.166:444/";
      "jws.toolkit.by.eli.kogan.wang" = "http://localhost:6000/";
      "ghost.${config.networking.domain}" = "http://localhost:2368/";
    };
    sslEverywhere = true;
  };
  services.nginx = {
    enable = true;
    enableReload = true;
    virtualHosts =
      let
        returnRoot = target: {
          addSSL = true;
          enableACME = true;
          locations = {
            "/" = {
              return = target;
            };
          };
        };
      in
      {
        "fun.${config.networking.domain}" = returnRoot "302 https://jws.toolkit.by.eli.kogan.wang/";
        "eli.kogan.wang" = returnRoot "302 https://eliko.ga/";
        "kogan.wang" = returnRoot "302 https://eliko.ga/";
        "coaf.in" = returnRoot "302 https://eliko.ga/";
        "jws.toolkit.by.eli.kogan.wang" = {
          addSSL = lib.mkForce false;
          forceSSL = true;
        };
        "${config.networking.domain}" = {
          default = true;
          forceSSL = true;
          enableACME = true;
          locations = {

            "/sso-auth" = {
              proxyPass = "http://localhost:8082/auth";
              extraConfig = ''
                proxy_pass_request_body off;
                proxy_set_header Content-Length "";
                proxy_set_header X-Origin-URI $request_uri;
                proxy_set_header X-Host $http_host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Application "net";
              '';
            };
            "/sso-logout" = {
              return = "302 https://login.6xr.de/logout?go=$scheme://$http_host/";
            };
            "@error401" = {
              return = "302 https://login.6xr.de/login?go=$scheme://$http_host$request_uri";
            };
          };
          extraConfig = ''
            error_page 401 = @error401;
          '';
        };
      };
    recommendedProxySettings = true;

  };
}
