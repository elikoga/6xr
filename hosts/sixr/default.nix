{ config, lib, suites, profiles, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./nginx
    ./gitea.nix
    ./mailserver.nix
    ./monitoring.nix
    ./ghost
    ./irc.nix
    ./misskey.nix
    ./containering.nix
    profiles.hydra
  ] ++ suites.base;

  services.mosquitto = {
    enable = true;
    allowAnonymous = true;
    # TODO: secure that shit
    aclExtraConf = ''
      pattern readwrite #
    '';
    users = { };
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";
  services.fail2ban.enable = true;

  networking.useDHCP = false;
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = true;
  networking.firewall.allowedTCPPorts = [ 22 80 443 ];
  networking.interfaces.ens18 = {
    useDHCP = true;
    ipv6 = {
      addresses = [
        {
          address = "2a02:c206:3007:5893::1";
          prefixLength = 64;
        }
      ];
      routes = [
        {
          address = "2a02:c206:3007:5893::1";
          prefixLength = 10;
          via = "fe80::1";
        }
      ];
    };
  };

  networking.domain = "6xr.de";

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  boot.kernel.sysctl = {
    "fs.inotify.max_user_watches" = 524288; # Good for Node development apparently
  };

  services.doh-proxy-rust = {
    enable = true;
    flags = [
      "--listen-address=127.0.0.1:3344"
    ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
