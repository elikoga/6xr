{ config, ... }:
{
  mailserver = {
    enable = true;
    fqdn = config.networking.domain;
    domains = [ config.networking.domain ];
    loginAccounts = {
      "coafin@${config.networking.domain}" = {
        hashedPassword = "$2y$10$PFnkQT7DLUtIiACVMhqJo.lsrv7dtX2NTJBFV/g4xyNnTdEY1ulsu";
        aliases = [ "@${config.networking.domain}" ];
        catchAll = [ "${config.networking.domain}" ];
      };
    };
    certificateScheme = 1;
    certificateFile = "/var/lib/acme/${config.networking.domain}/cert.pem";
    keyFile = "/var/lib/acme/${config.networking.domain}/key.pem";
  };
}
