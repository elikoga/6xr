{ ... }:
{
  services.znc = {
    enable = false;
    mutable = false; # Overwrite configuration set by ZNC from the web and chat interfaces.
    useLegacyConfig = false; # Turn off services.znc.confOptions and their defaults.
    openFirewall = true; # ZNC uses TCP port 5000 by default.
    config = {
      LoadModule = [ "adminlog" ];
      User.elikoga = {
        Admin = true;
        Method = "sha256"; # Fill out this section
        Hash = "b14116088564443075f88a187a22abe4c15e114393c382a8b50efc49a6568304"; # with the generated hash.
        Salt = "bT5Ef!JXrh*pJ;JzGi_X";
      };

    };
  };
}
