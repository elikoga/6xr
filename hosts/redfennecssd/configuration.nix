# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./vms.nix
    ];

  nixpkgs.config.allowUnfree = true;
  documentation.dev.enable = true;
  boot.plymouth.enable = true;
  boot.supportedFilesystems = [ "ntfs" ];
  boot.extraModulePackages = [
    # config.boot.kernelPackages.exfat-nofuse
    pkgs.linuxPackages.v4l2loopback
  ];
  boot.kernelModules = [
    "v4l2loopback"
  ];
  boot.extraModprobeConfig = ''
    options v4l2loopback exclusive_caps=1
  '';
  services.vnstat.enable = true;
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [
    gutenprint
    gutenprintBin
    hplip
    hplipWithPlugin
    splix
    brlaser
    brgenml1lpr
    brgenml1cupswrapper
  ];
  services.locate.enable = true;
  services.gnome.gnome-keyring.enable = true;
  services.gnome.glib-networking.enable = true;
  programs.seahorse.enable = true;
  programs.dconf.enable = true;
  services.gvfs.enable = true;
  programs.adb.enable = true;
  environment.pathsToLink = [ "/libexec" ];
  environment.variables.TERMINAL = "kitty";
  environment.variables._JAVA_OPTIONS = "-Dawt.useSystemAAFontSettings=lcd";

  environment.variables._JAVA_AWT_WM_NONREPARENTING = "1";

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  security.sudo.wheelNeedsPassword = false;
  users.mutableUsers = false;
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
    noto-fonts
  ];
  virtualisation.podman.enable = true;
  virtualisation.podman.dockerSocket.enable = true;
  virtualisation.podman.defaultNetwork.dnsname.enable = true;
  security.pam.services.coafin.enableGnomeKeyring = true;
  programs.steam.enable = true;
  time.hardwareClockInLocalTime = true;

  xdg.portal.enable = false;
  xdg.portal.gtkUsePortal = false;

  boot.initrd.kernelModules = [ "amdgpu" ];
  services.xserver.videoDrivers = [ "amdgpu" "intel" ];


  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true; # so that gtk works properly
    extraPackages = with pkgs; [
      swaylock
      swayidle
      xwayland
      wl-clipboard
      mako # notification daemon
      kitty # Alacritty is the default terminal in the config
      wofi # Dmenu is the default in the config but i recommend wofi since its wayland native
    ];
  };

  networking.hosts = {
    "127.0.0.1" = [ "redfennecssd.local" ];
  };
  services.nextcloud = {
    enable = true;
    hostName = "redfennecssd.local";
    config.adminpassFile = "/etc/secrets/nextcloud.adminpassFile";
  };
  networking.interfaces.enp7s0 = {
    ipv4.addresses = [
      {
        "address" = "192.168.0.2";
        "prefixLength" = 24;
      }
    ];
  };


  # services.xserver.libinput = {
  #   enable = true;
  #   scrollMethod = "button";
  #   scrollButton = 2;
  #   middleEmulation = false;
  # };

  boot.loader.grub = {
    enable = true;
    version = 2;
    devices = [ "nodev" ];
    efiSupport = true;
    useOSProber = true;
  };


  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # networking.hostName = "redfennec"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp7s0.useDHCP = true;
  networking.interfaces.enp0s20f0u13u4.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  services.xserver.displayManager = {
    lightdm.enable = true;
  };
  # services.xserver.displayManager.gdm.enable = true;
  # services.xserver.desktopManager.gnome3.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "us,de";
  services.xserver.xkbOptions = "caps:super,grp:shifts_toggle";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.coafin = {
    isNormalUser = true;
    extraGroups = [ "wheel" "podman" "tty" "dialout" "libvirtd" ]; # Enable ‘sudo’ for the user.
    hashedPassword = lib.mkForce "$6$ixcLHZOIyPAGS$bH4hWJrMQrzgzVGR.BmCRD1qxY1OLOpuOoQ3kr4RDrzsA9dISCgeliOh14c34t/e2btSzvIyI57ibqhCJtD451";
  };

  # users.users.abigail = {
  #  isNormalUser = true;
  #   hashedPassword = "$6$sar2GC61hS2isg$kKZVtDtC/q58DLTgA0b18ZkTB7QpvKRpClocVvkPUG5CGxrslLEKB64dyYVXjFM45VjmqCy/q5OASDglfaBxi.";
  # };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    arandr
    bitwarden
    bitwarden-cli
    calibre
    discord
    podman-compose
    dunst
    file
    firefox
    git
    git-lfs
    i3blocks
    killall
    kitty
    libnotify
    libreoffice
    lightlocker
    lxrandr
    maim
    mpc_cli
    mpd
    neovim
    netbeans
    networkmanagerapplet
    nixpkgs-fmt
    obs-studio
    pcmanfm
    pfetch
    picom
    polkit_gnome
    pulsemixer
    pwgen
    pywal
    ranger
    rofi
    sxhkd
    tdesktop
    thefuck
    thunderbird
    unzip
    vscode
    wget
    xcape
    xdotool
    xorg.xmodmap
    zathura
    zip
  ] ++ (
    with gnome; [
      adwaita-icon-theme
      file-roller
      dconf-editor
    ]
  );

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 80 ];
  networking.firewall.allowedUDPPorts = [ 80 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

