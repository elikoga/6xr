{ pkgs, ... }:
{
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  environment.systemPackages = with pkgs; [ virt-manager virt-viewer OVMF ];
  virtualisation.spiceUSBRedirection.enable = true;
}
