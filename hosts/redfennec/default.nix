{ suites, lib, ... }:
{
  imports = [
    ./configuration.nix
  ] ++ suites.base;

  nix.gc.automatic = lib.mkForce false;
  nix.distributedBuilds = true;
  nix.buildMachines = [
    {
      hostName = "6xr.de";
      system = "x86_64-linux";
      maxJobs = 4;
      speedFactor = 1;
      supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
      mandatoryFeatures = [ ];
      sshKey = "/root/.ssh/id_ed25519_root_redfennec_nixbuilds";
    }
    {
      hostName = "vps.eliko.ga";
      system = "x86_64-linux";
      maxJobs = 4;
      speedFactor = 1;
      supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
      mandatoryFeatures = [ ];
      sshUser = "admin";
      sshKey = "/root/.ssh/id_ed25519_root_redfennec_nixbuilds";
    }
  ];
  nix.extraOptions = ''
    builders-use-substitutes = true
  '';
}
