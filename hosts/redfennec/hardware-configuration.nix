# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/37117717-148c-485c-a1d5-23c1f0ee9d6d";
      fsType = "btrfs";
    };

  boot.initrd.luks.devices."cryptroot".device = "/dev/disk/by-uuid/3f6a2a1f-8495-493c-b0e8-4f02cbd1ec2b";

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/1F9E-63A5";
      fsType = "vfat";
    };

  fileSystems."/var/lib/docker/btrfs" =
    {
      device = "/var/lib/docker/btrfs";
      fsType = "none";
      options = [ "bind" ];
    };

  swapDevices =
    [{ device = "/dev/disk/by-uuid/7d4e6129-789e-4532-ad90-0466d1d1f757"; }];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
