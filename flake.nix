{
  description = "A highly structured configuration database.";

  inputs =
    {
      nixos.url = "github:NixOS/nixpkgs/nixos-21.05";
      latest.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
      digga.url = "github:elikoga/digga/master";
      nur.url = "github:nix-community/NUR";
      home.url = "github:nix-community/home-manager/release-21.05";
      home.inputs.nixpkgs.follows = "nixos";
      naersk.url = "github:nmattia/naersk";
      naersk.inputs.nixpkgs.follows = "latest";
      agenix.url = "github:ryantm/agenix";
      agenix.inputs.nixpkgs.follows = "latest";
      nixos-hardware.url = "github:nixos/nixos-hardware";

      simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver";

      pkgs.url = "path:./pkgs";
      pkgs.inputs.nixpkgs.follows = "nixos";

      ssh-keys-file.url = "path:./ssh-keys.nix";
      ssh-keys-file.flake = false;
    };

  outputs =
    { self
    , pkgs
    , digga
    , nixos
    , home
    , nixos-hardware
    , nur
    , agenix
    , ssh-keys-file
    , simple-nixos-mailserver
    , ...
    } @ inputs:
    let
      ssh-keys = import ssh-keys-file;
    in
    digga.lib.mkFlake {
      inherit self inputs ssh-keys;

      supportedSystems = [ "x86_64-linux" ];

      channelsConfig = { allowUnfree = true; };

      channels = {
        nixos = {
          imports = [ (digga.lib.importers.overlays ./overlays) ];
          overlays = [
            ./pkgs/default.nix
            pkgs.overlay # for `srcs`
            nur.overlay
            agenix.overlay
          ];
        };
        latest = { };
      };

      lib = import ./lib { lib = digga.lib // nixos.lib; };

      sharedOverlays = [
        (final: prev: {
          lib = prev.lib.extend (lfinal: lprev: {
            our = self.lib;
          });
        })
      ];

      nixos = {
        hostDefaults = {
          system = "x86_64-linux";
          channelName = "nixos";
          modules = ./modules/module-list.nix;
          externalModules = [
            { _module.args.ourLib = self.lib; }
            home.nixosModules.home-manager
            agenix.nixosModules.age
            simple-nixos-mailserver.nixosModule
            ./modules/customBuilds.nix
          ];
        };

        imports = [ (digga.lib.importers.hosts ./hosts) ];
        hosts = {
          /* set host specific properties here */
          "sixr" = { };
        };
        importables = rec {
          inherit ssh-keys;
          profiles = digga.lib.importers.rakeLeaves ./profiles // {
            users = digga.lib.importers.rakeLeaves ./users;
          };
          suites = with profiles; rec {
            base = [ core users.coafin users.root ];
          };
        };
      };

      home = {
        modules = ./users/modules/module-list.nix;
        externalModules = [ ];
        importables = rec {
          profiles = digga.lib.importers.rakeLeaves ./users/profiles;
          suites = with profiles; rec {
            base = [ direnv git ];
          };
        };
      };

      devshell.modules = [ ./modules/devshell.nix ];

      devshell.externalModules = { pkgs, ... }: {
        packages = [ pkgs.agenix ];
      };

      homeConfigurations = digga.lib.mkHomeConfigurations self.nixosConfigurations;

      deploy.nodes = nixos.lib.attrsets.recursiveUpdate (digga.lib.mkDeployNodes self.nixosConfigurations { }) {
        sixr.sshUser = "root";
      };

      defaultTemplate = self.templates.flk;
      templates.flk.path = ./.;
      templates.flk.description = "flk template";

    }
  ;
}
